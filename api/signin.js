var api = require('../db-api');
var restrict = require('../lib/restrict');
var normalizeEmail = require('../lib/normalize-email');
var log = require('debug')('pollu:signin-api');
var express = require('express');
var app = module.exports = express();
var jwt = require('../lib/jwt');

var mongoose = require('mongoose');
var User = mongoose.model('User');

var auth = User.authenticate();

/**
 * @api POST path /api/signin
 *   Basic signin pathway
 * @apiParam {String} email
 * @apiParam {String} password
 * @apiSuccess {Object} user
 *   user object
 * @apiSuccess {String} token 
 *   jwt token required as header for restricted pathways
 */
app.post('/signin', function (req, res) {
  log('Request POST /signin');
  var email = normalizeEmail(req.body.email);
  var password = req.body.password;

  auth(email, password, (err, user, info) => {
    if (err) return handleError(err, res);

    if (info && info.name == 'IncorrectPasswordError') {
      log('Incorrect password for user with email %s', email);
      return res.json({
        error: 'Incorrect Password'
      });
    }

    if (!user) {
      log('User with email %s does not exist', email);
      return res.json({
        error: 'User not found'
      });
    }

    if (!user.validated) {
      log('User with email %s has an unvalidated email', email);
      return res.json({
        error: 'Email not validated'
      });
    }

    log('Authenticated user with email %s', email);
    return jwt.signin(user, req, res);
  });
});

function handleError(err, res) {
  log('Found error %s', err);
  return res.status(500).send(err);
}