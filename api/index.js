var express = require('express');
var app = module.exports = express();

app.use(require('./survey'));
app.use(require('./signin'));
app.use(require('./signup'));
app.use(require('./user'));
app.use(require('./prize'));