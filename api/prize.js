var api = require('../db-api');
var restrict = require('../lib/restrict');
var expose = require('../lib/expose');
var log = require('debug')('pollu:prize-api');
var express = require('express');
var app = module.exports = express();

/**
 * @api GET path /api/prize/all
 *   Get all prizes
 * @apiHeader {String} X-ACCESS-TOKEN jwt token recieved with signin
 * @apiSuccess {Object} prizes 
 *   list of prizes
 */
app.get('/prize/all', restrict.users, function (req, res) {
  log('Request GET /prize/all');

  api.prize.all(req.user.id, (err, prizes) => {
    if (err) return handleError(err, res);

    log('Serving prizes %j', prizes.map(p => p.id));
    return res.json(prizes);
  });
});

app.post('/prize/claim/:id', restrict.users, function (req, res) {
  log('Request /prize/claim/%s', req.params.id);

  api.prize.claim(req.user, req.params.id, (err, user) => {
    if (err) return handleError(err, res);

    log('Successfully claimed prize. Serving updated user.');
    return res.json({user: expose.user(user)});
  });
});

function handleError(err, res) {
  log('Found error %j', err);
  return res.status(500).send(err);
}
