var api = require('../db-api');
var restrict = require('../lib/restrict');
var normalizeEmail = require('../lib/normalize-email');
var jwt = require('../lib/jwt');
var expose = require('../lib/expose');
var log = require('debug')('pollu:signup-api');
var express = require('express');
var app = module.exports = express();

var mongoose = require('mongoose');
var User = mongoose.model('User');

var auth = User.authenticate();

/**
 * @api POST path /api/signup
 *   Initial signup request
 * @apiParam {String} email
 * @apiParam {String} password
 * @apiSuccess {Object} response
 *   response.user = unvalidated user object
 */
app.post('/signup', function (req, res) {
  log('Request POST /signup');
  var email = normalizeEmail(req.body.email);
  var splitEmail = email.split('@')[0].split('.');

  var newUser = new User({
    email: email,
    validationCode: generateCode(),
    validated: false,
    firstName: splitEmail[0],
    lastName: splitEmail[splitEmail.length - 2],
    demographics: {
      year: splitEmail[splitEmail.length - 1]
    } 
  });

  User.register(newUser, req.body.password, (err, user) => {
    if (err) {
      if (err.name == 'UserExistsError') {
        log('User with email %s already exists. Proceceeding to validation.', newUser.email);
        return res.json({user: expose.user(newUser)});
      }

      return handleError(err, res);
    }

    log('Registered new user with email %s', user.email);

    emailCode(user.email, user.validationCode);
    log('Sent email with code to user %s', user.email);

    return res.json({user: expose.user(user)});
  });
});

/*
 * @api POST path /api/signup/resend
 *   Resend's the email with the validation code
 * @apiParam {String} email
 * @apiParam {String} password
 * @apiSuccess {Object} user
 *   user object
 */
app.post('/signup/resend', function (req, res) {
  log('Request POST /signup/resend');

  auth(req.body.email, req.body.password, (err, user) => {
    if (err) return handleError(err, res);

    if (!user) {
      log('User with email %s does not exist', email);
      return res.json({
        error: 'User not found'
      });
    }

    if (user.validated) {
      log('User with email %s is already validated', email);
      return jwt.signin(user, req, res);
    }

    emailCode(user.email, user.val)
    log('Resent email with code to user %s', email);
    return res.send(200);
  });
});

/*
 * @api POST path /api/signup/validate
 *   Validate the user's email
 * @apiParam {String} email
 * @apiParam {String} code validation code entered by user
 * @apiSuccess {Object} user
 *   user object
 * @apiSuccess {Object} token
 *   jwt token required for restricted pathways
 */  
app.post('/signup/validate', function (req, res) {
  log('Request POST /signup/validate');

  User.findOne({email: req.body.email}, (err, user) => {
    if (err) return handleError(err, res);

    if (!user) {
      log('User with email %s does not exist', req.body.email);
      return res.json({
        error: 'User not found'
      });
    }

    if (user.validated) {
      log('User with email %s is already validated', req.body.email);
      return jwt.signin(user, req, res);
    }

    if (user.validationCode != req.body.code) {
      log('Incorrect validation token %d does not equal %d', req.body.code, user.validationCode);
      return res.json({
        error: 'Incorrect validation code'
      });
    }

    user.validationCode = null;
    user.validated = true;
    user.save(err => {
      if (err) return handleError(err, res);

      log('Successfully validated user with email %s.', user.email);
      return jwt.signin(user, req, res);
    });
  });
});

function generateCode(length) {
  if (!length) length = 5;
  // for testing only
  return '00000';
  // var result = '';
  // while (result.length < length) {
  //   result += Math.floor((Math.random() * 10)).toString();
  // }
  // return result;
}

function emailCode(email, code) {
  // to write – sendgrid?
  return null;
}

function handleError(err, res) {
  log('Found error %s', err);
  return res.status(500).send(err);
}