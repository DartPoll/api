var api = require('../db-api');
var restrict = require('../lib/restrict');
var expose = require('../lib/expose');
var log = require('debug')('pollu:survey-api');
var express = require('express');
var app = module.exports = express();

/**
 * @api GET path /api/survey/all
 *   Get all surveys for the user
 * @apiHeader {String} X-ACCESS-TOKEN jwt token recieved with signin
 * @apiSuccess {Object} response
 *   list of surveys
 */
app.get('/survey/all', restrict.users, function (req, res) {
  log('Request GET /survey/all');

  api.survey.all(req.user.id, (err, surveys) => {
    if (err) return handleError(err, res);

    log('Serving surveys %j', surveys.map(s => s.id));
    return res.json(surveys);
  });
});

/**
 * @api GET path /api/survey/:id
 *   Get a single survey
 * @apiHeader {String} X-ACCESS-TOKEN jwt token recieved with signin
 * @apiSuccess {Object} survey 
 *   individual survey with answers bound
 *   survey.answers = list of answer objects for all users with identities hidden
 *   survey.userAnswers = list of answer objects for given user
 */
app.get('/survey/:id', restrict.users, function (req, res) {
  log('Request GET /survey/%s', req.params.id);

  api.survey.one(req.params.id, req.user.id, (err, survey) => {
    if (err) return handleError(err, res);

    log('Serving survey %s', survey.id);
    return res.json(survey);
  });
});

/**
 * @api POST path /api/survey/:id/answer
 *   Record a group of answers to a survey
 * @apiHeader {String} X-ACCESS-TOKEN jwt token recieved with signin
 * @apiParam {Object} answers - list of answer objects to every answer in the survey 
 * @apiSuccess {Object} user
 *   user object with updated reward points
 */
app.post('/survey/:id/answer', restrict.users, function (req, res) {
  log('Request POST /survey/%s/answer', req.params.id);

  api.answer.setAll(req.params.id, req.body.answers, req.user.id, (err, user) => {
    if (err) return handleError(err, res);

    log('Serving user %s', req.user.id);
    return res.json({user: expose.user(user)});
  });
});

/**
 * Respond with compressed total answers
 * CURRENTLY SURVING ANSWERS AND USERANSWERS AS PART OF GET /survey
 */
// app.get('/survey/:id/answers/all', restrict.users, function (req, res) {
//   log('Request GET /survey/%s/answers', req.params.id);

//   api.answer.all(req.params.id, (err, answers) => {
//     if (err) return handleError(err, res);

//     log('Serving answers for survey %s', req.params.id);
//     return res.json(answers);
//   });
// });

/**
 * Respond with the user's answers
 * CURRENTLY SURVING ANSWERS AND USERANSWERS AS PART OF GET /survey
 */
// app.get('/survey/:id/answers/mine', restrict.users, function (req, res) {
//   log('Request GET /survey/%s/answers/mine', req.params.id);

//   api.answer.mine(req.params.id, req.user.id, (err, answers) => {
//     if (err) return handleError(err, res);

//     log('Serving user\'s answers for survey %s', req.params.id);
//     return res.json(answers);
//   });
// });

/**
 * Create the new survey and serve it
 * Restricted to admins only
 */
app.post('/survey/create', restrict.admins, function (req, res) {
  log('Request POST /survey/create');

  api.survey.create(req.body.survey, (err, survey) => {
    if (err) return handleError(err, res);

    log('Serving newly created survey with id %s', survey.id);
    return res.json(survey);
  });
});

function handleError(err, res) {
  log('Found error %j', err);
  return res.status(500).send(err);
}
