var api = require('../db-api');
var restrict = require('../lib/restrict');
var expose = require('../lib/expose');
var log = require('debug')('pollu:user-api');
var express = require('express');
var app = module.exports = express();

/**
 * @api GET path /api/survey/id
 *   Get the user's data
 * @apiHeader {String} X-ACCESS-TOKEN jwt token recieved with signin
 * @apiSuccess {Object} user 
 *   user object
 */
app.get('/user/me', restrict.users, function (req, res) {
  log('Request GET /user/me');

  return res.json({user: expose.user(req.user)});
});

function handleError(err, res) {
  log('Found error %j', err);
  return res.status(500).send(err);
}
