# Pulse API

One API for web app and multi-platform mobile app.

## QuickStart

You need mongodb and node installed.

Install node and npm here: https://nodejs.org/en/
Install mongodb here: https://www.mongodb.org/downloads#production

Clone this repo, and cd into it. 

After that, make sure your directory structure is as follows:
```
Pulse/
--- app/
--- api/
```

You shouldn't need to `sudo` it, except for the first time. Then you'll need to do:
```
sudo mkdir -p /data/db
sudo chown -R $USER /data/db
```

And, make sure that you have mongodb running. To run it, type:
```
mongod
```


`mongod` usually does not exit whenever you close the terminal window. To determine if the database is currently running, type `mongo`, which will either take you into the database's shell or fail because the database is not running.

Now, you can go to this directory and type:
```
npm i
export DEBUG=pollu*
node app.js
```

And go to `localhost:3000` and you're good!

## DB Maintenance

To initialize the database with some surveys, make sure `mongod` is running and type:
```
db-maintenance/initialize-db.js
```

To clear the database, type `mongo` to enter the mongodb shell, and then type:
```
use pollu
db.dropDatabase()
```

## Docs

Clone the repo and open `docs/index.html` in your browser.