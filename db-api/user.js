var mongoose = require('mongoose');
var User = mongoose.model('User');
var log = require('debug')('pollu:db-api:user');

exports.givePoints = function (userId, numPoints, fn) {
  User
  .findById(userId)
  .exec((err, user) => {
    if (err) return log(err), fn(err);

    user.numPoints = user.numPoints + numPoints;
    user.save(err => {
      if (err) return log(err), fn(err);

      log('Gave %d points to user %s', numPoints, userId);
      return fn(null, user);
    });
  });
};

exports.takePoints = function (userId, numPoints, fn) {
  User
  .findById(userId)
  .exec((err, user) => {
    if (err) return log(err), fn(err);

    user.numPoints = user.numPoints - numPoints;
    user.save(err => {
      if (err) return log(err), fn(err);

      log('Took %d points from user %s', numPoints, userId);
      return fn(null, user);
    });
  });
};

exports.promiseTakePoints = function (userId, numPoints) {
  return new Promise((resolve, reject) => {
    exports.takePoints(userId, numPoints, (err, user) => {
      if (err) return reject(err);

      return resolve(user);
    });
  });
};
