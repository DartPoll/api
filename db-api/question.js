var mongoose = require('mongoose');
var Question = mongoose.model('Question');
var answerApi = require('./answer');
var log = require('debug')('pollu:db-api:question');
var copy = require('../lib/copy');

exports.create = function (data, fn) {
  var question = new Question(data);
  question.save(err => {
    if (err) return fn(err);

    log('Successfully created new question.');
    return fn(null, question);
  });
};

exports.promiseCreate = function (data) {
  return new Promise((resolve, reject) => {
    exports.create(data, (err, question) => {
      if (err) return reject(err);

      return resolve(question);
    });
  });
};

function promiseAnswers (question, userId) {
  return new Promise((resolve, reject) => {
    answerApi.allForQuestion(question, userId, (err, answers) => {
      if (err) return reject(err);

      var q = copy.question(question);
      q.answers = answers.all;
      q.userAnswers = answers.mine;

      return resolve(q);
    });
  });
};

exports.attachAnswers = function (questions, userId, fn) {
  var promises = questions.map(q => promiseAnswers(q, userId));

  Promise.all(promises)
  .then(qs => {
    return fn(null, qs);
  })
  .catch(err => {
    return fn(err);
  })
};
