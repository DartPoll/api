var mongoose = require('mongoose');
var Prize = mongoose.model('Prize');
var userApi = require('./user');
var log = require('debug')('pollu:db-api:user');

exports.all = function (userId, fn) {
  Prize
  .find()
  .sort()
  .exec((err, prizes) => {
    if (err) return log(err), fn(err);

    return fn(null, prizes);
  });
};

function promiseAddClaimer(prize, userId) {
  return new Promise((resolve, reject) => {
    prize.claimers.push(userId);
    prize.save(err => {
      if (err) return reject(err);

      log('Added user %s to prize %s claimers', userId, prize.id);
      return resolve(prize);
    });
  });
};

exports.claim = function (user, prizeId, fn) {
  Prize
  .findById(prizeId)
  .exec((err, prize) => {
    if (err) return log(err), fn(err);

    if (prize.cost > user.numPoints) {
      log('User %s has insufficient points to claim %s', user.id, prize.id);
      return fn('Insufficient points');
    }

    var takePointsPromise = userApi.promiseTakePoints(user.id, prize.cost);
    var addClaimerPromise = promiseAddClaimer(prize, user.id);

    Promise.all([takePointsPromise, addClaimerPromise])
    .then(userPrize => {
      var user = userPrize[0];
      return fn(null, user);
    })
    .catch(err => {
      return log(err), fn(err);
    });
  });
};

exports.create = function (data, fn) {
  var prize = new Prize(data);
  prize.save(err => {
    if (err) return log(err), fn(err);

    log('Successfully created new prize %j', prize);
    return fn(null, prize);
  });
};
