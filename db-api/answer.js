var mongoose = require('mongoose');
var Answer = mongoose.model('Answer');
var log = require('debug')('pollu:db-api:answer');
var surveyApi = require('./survey');
var userApi = require('./user');
var expose = require('../lib/expose');

exports.allForQuestion = function (questionId, userId, fn) {
  Answer
  .find({question: questionId})
  .populate('user')
  .exec((err, answers) => {
    if (err) return fn(err);

    var mine = answers.filter(a => a.user == userId).map(a => expose.answer(a));
    var processed = answers.map(a => expose.answer(a));

    log('Found answers %s', processed.map(a => a.id));
    return fn(null, {mine: mine, all: processed});
  });
};

function create (data, fn) {
  var answer = new Answer(data);
  answer.save(err => {
    if (err) return fn(err);

    log('Successfully created new answer.');
    return fn(null, answer);
  });
}

exports.set = function(surveyId, data, userId, fn) {
  var a = {
    user: userId,
    question: data.question,
    type: data.type,
    survey: surveyId,
    selections: data.selections,
    text: data.text,
    numVal: data.numVal
  };

  Answer
  .findOne({user: a.user, question: a.question})
  .exec((err, answer) => {
    if (err) return fn(err);

    if (!answer) return create(a, fn);

    answer.selections = a.selections;
    answer.text = a.text;
    answer.numVal = a.numVal;
    answer.save(err => {
      if (err) return fn(err);

      log('Successfully saved answer.');
      return fn(null, answer);
    });
  });
};

function promiseSet(surveyId, data, userId) {
  var promise = new Promise((resolve, reject) => {
    exports.set(surveyId, data, userId, (err, answer) => {
      if (err) return reject(err);

      return resolve(answer);
    });
  });

  return promise;
};

exports.setAll = function(surveyId, answers, userId, fn) {
  var answerPromises = answers.map(a => promiseSet(surveyId, a, userId));

  Promise.all(answerPromises)
  .then(answers => {

    /**
     * Need to add the user as a participant so that it gets marked as complete in the future
     */
    surveyApi.addParticipant(surveyId, userId, (err, survey) => {
      if (err) return log(err), fn(err);

      /**
       * Also, need to give the user the proper number of points
       */
      userApi.givePoints(userId, survey.pointValue, (err, user) => {
        if (err) return log(err), fn(err);

        return fn(null, user);
      });
    });


  })
  .catch(err => {
    return log(err), fn(err);
  });
};
