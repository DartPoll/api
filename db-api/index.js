var apisToExpose = 'survey answer prize'.split(' ');

apisToExpose.forEach(api => {
  exports[api] = require(`./${api}`);
});
