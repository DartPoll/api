var mongoose = require('mongoose');
var Survey = mongoose.model('Survey');
var questionApi = require('./question');
var log = require('debug')('pollu:db-api:survey');
var extend = require('util')._extend;

exports.all = function (userId, fn) {
  Survey
  .find()
  .select('id title description createdAt closesAt resultsAt imgSrc numParticipants participants pointValue wordCount')
  .exec((err, surveys) => {
    if (err) return log(err), fn(err);

    processed = surveys.map(s => processSurvey(s, userId));
    log('Found surveys %j', processed.map(s => s.id));
    return fn(null, processed);
  });
};

exports.one = function (surveyId, userId, fn) {
  Survey
  .findById(surveyId)
  .select('id title description createdAt closesAt resultsAt imgSrc numParticipants participants pointValue questions conclusions wordCount')
  .populate('questions')
  .exec((err, survey) => {
    if (err) return log(err), fn(err);
    if (!survey) return log('No survey found with id %s', surveyId), fn(null);

    survey = processSurvey(survey, userId);

    log('Found survey %s', survey.id);

    /**
     * If the survey is complete, attach answers
     */
    if (survey.complete) {
      log('Survey %s has been completed by user %s. Attaching answers', surveyId, userId);
      questionApi.attachAnswers(survey.questions, userId, (err, qs) => {
        survey.questions = qs;
        log('Answers attached.');
        return fn(null, survey);
      });
    } 

    else {
      return fn(null, survey);
    }
  });
};

function wordCount(data) {
  var count = data.description.length + data.title.length;
  data.questions.forEach(q => {
    count += q.description.length + q.options.length;
  });
  return count;
}

exports.create = function (data, fn) {
  data.wordCount = wordCount(data);
  var questionPromises = data.questions.map(q => questionApi.promiseCreate(q));

  Promise.all(questionPromises)
  .then(qs => {
    data.questions = qs.map(q => q.id);

    var survey = new Survey(data);
    survey.save(err => {
      if (err) return log(err), fn(err);

      log('Created new survey.');
      return fn(null, survey);
    });
  })
  .catch(err => {
    return log(err), fn(err);
  });
}

/**
 * Mark the survey as complete if the user has already taken it
 * Hide other participants from the user 
 * @param  {Object} survey
 * @param  {String} userId
 */
function processSurvey (survey, userId) {
  var copy = {};
  'id title description createdAt closesAt resultsAt imgSrc numParticipants participants pointValue questions conclusions wordCount'.split(' ').forEach(prop => {
    if (prop != 'participants' && survey[prop] != null) 
      copy[prop] = survey[prop];
  });

  copy.complete = survey.participants.indexOf(userId) > -1;
  return copy;
};

exports.addParticipant = function (surveyId, userId, fn) {
  Survey
  .findById(surveyId)
  .exec((err, survey) => {
    if (err) return fn(err);

    if (!survey) return log('Survey with id %s not found.', surveyId), fn(null);

    if (survey.participants.indexOf(userId) > -1) {
      return log('User %s already a participant for survey %s', userId, surveyId), fn(null, survey);
    }

    survey.participants.push(userId);
    survey.save(err => {
      if (err) return fn(err);

      return fn(null, survey);
    });
  });
};

