var sampleData = require('./sample-data');
var express = require('express');
var app = module.exports = express();
var log = require('debug')('pollu:initailize-db');

/**
 * Register Models and Launch Mongoose
 */

require('../models')(app);

var mongoose = require('mongoose');
var api = require('../db-api');

var sampleSurveys = sampleData.surveys;
var samplePrizes = sampleData.prizes;

var promises = sampleSurveys.map(s => promiseSurvey(s));
Promise.all(promises)
.then(surveys => {

})
.catch(err => {
  log(err);
});

var promises = samplePrizes.map(p => promisePrize(p));
Promise.all(promises)
.then(prizes => {
  process.exit();
})
.catch(err => {
  log(err);
  process.exit();
}) 

function promiseSurvey(data) {
  return new Promise((resolve, reject) => {
    api.survey.create(data, (err, survey) => {
      log('Successfully added new survey %s', survey.id);
      resolve(survey);
    });
  });
}

function promisePrize(data) {
  return new Promise((resolve, reject) => {
    api.prize.create(data, (err, prize) => {
      log('Successfully added new prize %s', prize.id);
      resolve(prize);
    });
  });
}