var moment = require('moment')

function stringToOption(string) {
  return {
    value: string,
    text: string
  };
}

module.exports.surveys = [ 
  {
    title: 'Sample Survey',
    description: 'Hello this is a sample survey',
    imgSrc: 'https://scontent-ord1-1.xx.fbcdn.net/hphotos-xat1/t31.0-8/12633596_10204887995873827_9037124484356194153_o.jpg',
    pointValue: 30,
    createdAt: new Date(),
    closesAt: new Date(moment(new Date()).add(7, 'days').toDate()),
    resultsAt: new Date(moment(new Date()).add(8, 'days').toDate()),
    questions: [ 
      {
        description: 'What do you think Number One?',
        type: 'mc',
        maxSelect: 1,
        options: [ 'A', 'B' ].map(o => stringToOption(o))
      },
      {
        description: 'Frank? Is that you?',
        type: 'mc',
        maxSelect: 2,
        options: [ 'Yes', 'No', 'Helen? How did you get in this app?' ].map(o => stringToOption(o))
      }
    ],
    conclusions: [
      '71% of respondents think that grapes taste better than Churches run',
      '20% of respondents eat breakfast three times',
      'No one cares more about you than the people you love',
      'Haha! Thanks!'
    ]
  },
  {
    title: 'Friendly Survey',
    description: 'Hello this is a friendly survey',
    imgSrc: 'https://1800hocking.files.wordpress.com/2011/07/hi-ohio-logo.jpg',
    pointValue: 20,
    createdAt: new Date(),
    closesAt: new Date(moment(new Date()).add(7, 'days').toDate()),
    resultsAt: new Date(),
    questions: [ {
      description: 'I am your friend!',
      type: 'mc',
      maxSelect: 2,
      options: [ '1', '2' ].map(o => stringToOption(o))
    } ]
  },
  {
    title: 'Complete Survey',
    description: 'Hello this is a survey thats fake completed',
    imgSrc: 'http://www.apriso.com/blog/wp-content/uploads/2011/12/complete_lean2.png',
    pointValue: 80,
    createdAt: new Date(),
    closesAt: new Date(moment(new Date()).add(7, 'days').toDate()),
    resultsAt: new Date(moment(new Date()).add(8, 'days').toDate()),
    questions: [ {
      description: 'You have already completed me',
      type: 'mc',
      maxSelect: 2,
      options: [ 'Frank', 'Polish' ].map(o => stringToOption(o))
    } ]
  }
];

module.exports.prizes = [
  {
    title: 'Pong with Terren'
  , description: 'Have Pong with Terren. Terren will play pong with you if you use his app.'
  , imgSrc: 'https://upload.wikimedia.org/wikipedia/commons/e/e1/Baby_Face.JPG'
  , cost: 15
  },
  {
    title: 'Lunch with Terren'
  , description: 'Eat lunch with Terren. Terren is lonely. Hop Foco or Collis, you choose. He\' not taking classes so he\'s free when you are.'
  , imgSrc: 'http://www.whatsthatbug.com/wp-content/uploads/2010/10/wolf_spider_cyprus_alex.jpg'
  , cost: 50
  }
];
