/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var log = require('debug')('pollu:question');

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var OptionSchema = new Schema({
    value: { type: String, required: true }
  , text: { type: String, required: true }
});

/**
 * Question Schema
 */
var QuestionSchema = new Schema({
    type: { type: String, enum: ['mc', 'slider', 'rank', 'text', 'dd', 'yn', 'matrix'] }
  , description: { type: String, required: true }
  , optional: { type: Boolean, default: true }
  , options: [OptionSchema] 
    // will have only if it's mc, dd, yn, or matrix
  , maxSelect: { type: Number, default: 1 }
    // if maxSelect > 1, checkboxes instead of radio types
  , input: { type: Boolean, default: false } 
    // if true and text, it's text entry question
    // if true and mc, there's an other option
  , dependentOn: { type: ObjectId, ref: 'Question', default: null }
    // null if question will be asked regardless of the answer to other questions
    // if not null, question is dependent on `dependentOn`'s value
    // if dependent on 
  , dependentValue: { type: String, default: null }
    // value to check whether question should be displayed
    // if dependentValue is null, dependence check won't happen / will always be true,
    // but this question will be guaranteed to come after `dependentOn`
  , range: [{type: Number}]
});


QuestionSchema.virtual('id').get(function () {
  return this._id;
});

QuestionSchema.set('toObject', {getters: true, virtuals: true});
QuestionSchema.set('toJSON', {getters: true, virtuals: true});


module.exports = function initialize(conn) {
  return conn.model('Question', QuestionSchema);
};