/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var passportLocalMongoose = require('passport-local-mongoose');
var log = require('debug')('pollu:models:user');

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var DemographicsSchema = require('./demographics');

/**
 * User Schema
 */

var UserSchema = new Schema({
    firstName: { type: String, required: true }
  , lastName: { type: String, required: true }
  , validated: { type: Boolean, default: false }
  , validationCode: { type: String, required: false }
  , numPoints: { type: Number, default: 0 }
  , admin: { type: Boolean, default: false }
  , demographics: { type: DemographicsSchema }
});

UserSchema.plugin(passportLocalMongoose, {
  usernameField: 'email'
});

UserSchema.set('toObject', {getters: true});
UserSchema.set('toJSON', {getters: true});

/**
 * Remove the hasn and salt of every document before returning the result
 */

UserSchema.options.toObject.transform = UserSchema.options.toJSON.transform = function(doc, ret, options) {
  delete ret.hash;
  delete ret.salt;
};

module.exports = function initialize(conn) {
  return conn.model('User', UserSchema);
};
