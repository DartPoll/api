The User model stores the biographical information about each person.

The Question model stores the question type as a string, the text question itself, additional descriptive information about the question, options for the answer if it's a multiple choice, drop down, or matrix question, and a few booleans about the question type. 
```
if type is 'mc':
    maxSelect must be 1 or greater.
    if 1:
        It's a radio question
    if greater:
        It's a checkbox question

    input can be true or false
    if true:
        there is an "other" option
    if false:
        there is no other option

if type is 'dd':
    maxSelect must be 1
    input can be true or false

if type is 'slider':
    slider goes between range[0] and range[1]
```

The Answer model stores a reference to the question it is an answer to, the survey that question is a part of, the type of question it is an answer to, and the data about the answer to the question.

```
if type is 'mc':
    the answer will be in `selections`, an array of strings
    the strings will correspond to the question's options' value
    selections.length <= maxSelect, > 0 if question is required

if type is 'rank order':
    answer will be in `selections`, same as above, except:
        the order of the values in selections will be important

if question's input is true:
    `text` may be set.
    if the type is text entry, it must be set.

if type is 'slider':
    `numVal` will be set
```

The Survey model stores a reference to each of its questions and anybody that has answered the survey.

With this set up, you'll be able to get, for example, all genderqueer jewish white people's answer to a particular survey by doing:
```
Answer
.find({
    surveyId: `surveyId`
})
.populate('user')
.exec((err, answers) => {
    genderqueerJews = answers.filter((a) => a.user.gender == 'genderqueer' && a.user.religion == 'jewish');
})
```

