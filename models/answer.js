/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var log = require('debug')('pollu:models:answer');

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var DemographicsSchema = require('./demographics');

/**
 * Answer Schema
 */
var AnswerSchema = new Schema({
    user: { type: ObjectId, ref: 'User', required: true }
  , question: { type: ObjectId, ref: 'Question', required: true }
  , survey: { type: ObjectId, ref: 'Survey', required: true }
  , type: { type: String, enum: ['mc', 'slider', 'rank', 'text', 'dd', 'yn', 'matrix'] }
  , text: { type: String, required: false }
  , selections: [{ type: String }]
  , numVal: { type: Number, required: false }
  , demographics: { type: DemographicsSchema }
});

module.exports = function initialize(conn) {
  return conn.model('Answer', AnswerSchema);
};