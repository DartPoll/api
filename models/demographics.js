/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

/**
 * Demographics Schema
 */

var DemographicsSchema = new Schema({
    year: { type: String, enum: ['12', '13', '14', '15', '16', '17', '18', '19', '20', 'GR'] }
  , gender: { type: String, enum: ['male', 'female', 'transgender', 'genderqueer'] }
  , race: { type: String, enum: ['native', 'asian', 'black', 'latino', 'white', 'multiple', 'other'] }
  , affiliation: { type: String, enum: ['fraternity', 'sorority', 'coed', 'none'] }
  , athlete: { type: Boolean, default: false }
  , religion: { type: String, enum: ['none', 'atheist', '...]'] }
});

module.exports = DemographicsSchema;