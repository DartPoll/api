/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var log = require('debug')('pollu:models:survey');

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

/**
 * Survey Schema
 */
var SurveySchema = new Schema({
    author: { type: ObjectId, ref: 'User' }
  , title: { type: String, required: true }
  , description: { type: String }
  , createdAt: { type: Date, default: Date.now() }
  , closesAt: { type: Date, default: null, required: false }
  , resultsAt: { type: Date, default: Date.now() }
  , pointValue: { type: Number, required: true }
  , imgSrc: { type: String, required: true } // more detailed validation here TODO
  , participants: [{type: ObjectId, ref: 'User' }]
  , questions: [{type: ObjectId, ref: 'Question'}]
  , conclusions: [{type: String}]
  , wordCount: { type: Number, required: true }
});

SurveySchema.virtual('numParticipants').get(function () {
  return this.participants.length;
});

SurveySchema.virtual('id').get(function () {
  return this._id;
});

SurveySchema.set('toObject', {getters: true, virtuals: true});
SurveySchema.set('toJSON', {getters: true, virtuals: true});

module.exports = function initialize(conn) {
  return conn.model('Survey', SurveySchema);
};
