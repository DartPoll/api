/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var log = require('debug')('pollu:prize');

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

/**
 * Prize Schema
 */
var PrizeSchema = new Schema({
    title: { type: String, required: true }
  , description: { type: String, required: true }
  , imgSrc: { type: String, required: true }
  , cost: {type: Number, required: true }
  , claimers: [{type: ObjectId, ref: 'User'}]
});

PrizeSchema.virtual('id').get(function () {
  return this._id;
});

PrizeSchema.set('toObject', {getters: true, virtuals: true});
PrizeSchema.set('toJSON', {getters: true, virtuals: true});


module.exports = function initialize(conn) {
  return conn.model('Prize', PrizeSchema);
};