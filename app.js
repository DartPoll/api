var log = require('debug')('pollu:app');
var express = require('express');
var app = module.exports = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

var jwtSecret = process.env.JWT_SECRET || '123456';

app.use(cookieParser(jwtSecret));
app.use(bodyParser.json());
app.use(express.static('../app/www'));

/**
 * Register Models and Launch Mongoose
 */

require('./models')(app);

/**
 * Add passport's authentication
 */

var auth = require('./lib/passport');
auth(app);

/**
 * Middleware to set req.user
 */

var jwt = require('./lib/jwt');
app.use(jwt.middleware);

/**
 * Define endpoints
 */

app.use('/api', require('./api'));

/**
 * Serve html
 */

app.get('/', function (req, res) {
  res.render('../app/www/index.html');
});

/*
 * Start the server
 */ 

var PORT = process.env.PORT;
if (!PORT) {
  log('Missing env var PORT, using 3000');
  PORT = 3000;
}

log('Listening on PORT %d', PORT);
app.listen(PORT);