var qprops = 'type description optional options maxSelect input dependentOn dependentValue range'.split(' ');

module.exports.question = function (q) {
  var copy = {};
  qprops.forEach(prop => {
    copy[prop] = q[prop];
  });
  return copy;
};