var log = require('debug')('pollu:jwt');
var moment = require('moment');
var mongoose = require('mongoose');
var jwt = require('jwt-simple');
var expose = require('../lib/expose');
var User = mongoose.model('User');

var jwtSecret = process.env.JWT_SECRET || '123456';

exports.signin = function signin(user, req, res) {
  req.user = user;
  return res.json(encode(user));
};

exports.middleware = function (req, res, next) {
  var encoded = req.headers['x-access-token'];

  if (encoded) {
    decode(encoded, (err, user, decoded) => {
      if (err || !user) {
        if (err) log('Error decoding token: %s', err);
        if (!user) log('No user found for jwt %s', encoded);

        return next();
      }

      log('Logging in user %s', user.id);
      req.login(user, function(err) {
        if (err) return res.json(500, { error: err.message });
        next();
      });
    });
  } else {
    log('HTTP header x-access-token has no token. Moving on...');
    return next();
  }
};

function encode (user) {
  user = expose.user(user);

  var expires = moment().add(15, 'days').valueOf();

  var token = jwt.encode({
    iss: user.email,
    exp: expires
  }, jwtSecret);

  return {
    token : token,
    user: user
  };
};

function decode (encoded, fn) {
  try {
    log('Attempting to decode token...');
    var decoded = jwt.decode(encoded, jwtSecret);

    if (decoded.exp <= Date.now()) {
      log('Access token has expired');
      return fn(new Error('Access token has expired'));
    }

    User.findByUsername(decoded.iss, function(err, user) {
      if (err) log('Token has been decoded, but user fetching failed with the following error: %s', err);
      if (!user) return log('Token has been decoded, but user was not found'), fn(new Error('No user for token %s', encoded));

      log('Token decoded successfully');
      return fn(null, user, decoded);
    });
  } catch (err) {
    log('Cannot decode token: %s', err);
    return fn(err);
  }
}
