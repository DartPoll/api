var log = require('debug')('pollu:restrict');

/**
 * Basic access restriction middleware
 * for authenticated users.
 */

module.exports.users = function (req, res, next) {
  log('Checking for logged in user');
  if (req.user) {
    log('User logged in, moving on...');
    next();
  } else {
    log('User is not logged in. Endpoint %s is restricted.', req.path);
    return res.status(403).json({
      error: 'Forbidden access'
    });
  }
};

module.exports.admins = function (req, res, next) {
  log('Checking for logged in user');
  if (req.user) {
    log('User logged in, checking if admin...');
    if (req.user.admin) {
      log('User is admin, moving on...');
      return next();
    } else {
      log('User is not an admin. Endoing %s is restricted.', req.path);
      return res.status(403).json({
        error: 'Forbidden access'
      });
    }
  } else {
    log('User is not logged in. Endpoint %s is restricted.', req.path);
    return res.status(403).json({
      error: 'Forbidden access'
    });
  }
};
