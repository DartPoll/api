module.exports.user = function (user) {
  return {
    email: user.email,
    firstName: user.firstName,
    lastName: user.lastName,
    validated: user.validated || false,
    numPoints: user.numPoints
  };
};

module.exports.answer = function (answer) {
  return {
    id: answer.id,
    question: answer.question,
    survey: answer.survey,
    type: answer.type,
    text: answer.text,
    selections: answer.selections,
    numVal: answer.numVal,
    demographics: answer.user.demographics,
  };
};